set encoding=UTF-8

if &compatible
    set nocompatible
end

call plug#begin('~/.vim/plugged')

" General helpers
Plug 'tpope/vim-sensible'
Plug 'ctrlpvim/ctrlp.vim' " file search

" tmux
Plug 'tmux-plugins/vim-tmux-focus-events'
Plug 'roxma/vim-tmux-clipboard'

call plug#end()

colorscheme industry

set spelllang=en_GB
" display incomplete commands
set showcmd

" I always set my virtual terminals to have a black background
if &term == "xterm"
    set background=dark
endif

" Line numbers
set number
nnoremap <F2> :set nonumber!<CR>:set foldcolumn=0<CR>

" In many terminal emulators the mouse works just fine, thus enable it (except over SSH, where it's
" just annoying).
if has('mouse') && $SSH_CONNECTION == ""
    set mouse=a
endif

" Highlight search terms when colours are available
if &t_Co > 2 || has("gui_running")
    set hlsearch
endif

" In practice, I almost never want to type an actual tab
set softtabstop=4
set shiftwidth=4
set shiftround
set expandtab

set modeline

" Make it obvious where 100 characters is
set textwidth=100
autocmd FileType python set textwidth=88 " match Black
set colorcolumn=+1

" Convenient command to see the difference between the current buffer and the
" file it was loaded from, thus the changes you made.
" Only define it when not defined already.
if !exists(":DiffOrig")
    command DiffOrig vert new | set bt=nofile | r # | 0d_ | diffthis
                     \ | wincmd p | diffthis
endif

let g:netrw_liststyle=3

" Treat <li> and <p> tags like the block tags they are
let g:html_indent_tags = 'li\|p'

" Open new split panes to right and bottom, which feels more natural
set splitbelow
set splitright

" Always use vertical diffs
set diffopt+=vertical

" Display extra whitespace
set list
set listchars=tab:→\ ,trail:·,nbsp:·

" Use one space, not two, after punctuation.
set nojoinspaces

" Allow triple-slash (doxygen) comments for C++ code.
autocmd Filetype cpp set comments^=:///

" Riggrep is fast: use it for searching
if executable('rg')
    set grepprg=rg\ -H\ --no-heading\ --vimgrep\ --hidden
    set grepformat=%f:%l:%c:%m

    " Use rg in CtrlP for listing files. Lightning fast and respects .gitignore
    let g:ctrlp_user_command = 'rg %s --files --color=never --glob ""'

    " rg is fast enough that CtrlP doesn't need to cache
    let g:ctrlp_use_caching = 0
endif
let g:ctrlp_working_path_mode = 'ra'
let g:ctrlp_clear_cache_on_exit = 1

let g:clang_format#detect_style_file=1

" Cursor in terminal
" https://vim.fandom.com/wiki/Configuring_the_cursor
" 1 or 0 -> blinking block
" 2 solid block
" 3 -> blinking underscore
" 4 solid underscore
" Recent versions of xterm (282 or above) also support
" 5 -> blinking vertical bar
" 6 -> solid vertical bar

if &term =~ 'screen\|^xterm'
  " normal mode
  let &t_EI .= "\<Esc>[0 q"
  " insert mode
  let &t_SI .= "\<Esc>[6 q"
  autocmd VimLeave * silent !echo -ne "\033[5 q"
endif
