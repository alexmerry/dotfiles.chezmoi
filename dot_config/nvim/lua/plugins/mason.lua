return {
  {
    "williamboman/mason.nvim",
    opts = {
      ensure_installed = {
        -- NB: servers set up in nvim-lspconfig don't need to be listed here
        -- formatters used by conform.nvim
        "black",
        "isort",
        "shfmt",
        "stylua",
      },
    },
  },
}
