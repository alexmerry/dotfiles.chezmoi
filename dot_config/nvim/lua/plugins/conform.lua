return {
  {
    "stevearc/conform.nvim",
    opts = {
      formatters_by_ft = {
        python = { "isort", "black" },
        cpp = { "clang-format" },
      },
      formatters = {
        ["clang-format"] = {
          condition = function(ctx)
            return vim.fs.find({ ".clang-format" }, { path = ctx.filename, upward = true })[1]
          end,
        },
      },
    },
  },
}
