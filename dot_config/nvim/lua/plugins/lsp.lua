return {
  {
    "neovim/nvim-lspconfig",
    opts = {
      servers = {
        clangd = {},
        pylsp = {
          settings = {
            pylsp = {
              plugins = {
                -- use black for formatting, so disable code style and formatting plugins
                pycodestyle = { enabled = false },
                autopep8 = { enabled = false },
                YAPF = { enabled = false },
              },
            },
          },
        },
        rust_analyzer = {},
      },
    },
  },
}
