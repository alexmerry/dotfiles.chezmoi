return {
  {
    "MagicDuck/grug-far.nvim",
    keys = {
      {
        "<leader>sr",
        function()
          local grug = require("grug-far")
          local ext = vim.bo.buftype == "" and vim.fn.expand("%:e")
          local filter = nil
          if ext and ext ~= "" then
            if ext == "h" or ext == "cpp" then
              filter = "*.{h,cpp}"
            else
              filter = "*." .. ext
            end
          end
          grug.open({
            transient = true,
            prefills = {
              filesFilter = filter,
            },
          })
        end,
        mode = { "n", "v" },
        desc = "Search and Replace",
      },
    },
  },
}
